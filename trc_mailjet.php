<?php 
/*
Plugin Name: TRC Mailjet
Description: Custom mailjet implementation
Author: Pete Gaulton - pete@gaulton.me
Version: 1.0
*/

// Declare settings page
function trcm_admin_actions() {
    add_options_page('Settings', 'TRC Mailjet', 'manage_options', 'trc-mailjet', 'trcm_settings');
} 
add_action('admin_menu', 'trcm_admin_actions');




// Settings page
function trcm_settings() {
    // Handle posted form
    if($_POST['trcm_hidden'] == 'Y') {
        $trcmFromName = trim($_POST['trcm_from_name']);
        update_option('trcm_from_name', $trcmFromName);
        $trcmFromEmail = trim($_POST['trcm_from_email']);
        update_option('trcm_from_email', $trcmFromEmail);

        $trcmApiKey = trim($_POST['trcm_key_api']);
        update_option('trcm_key_api', $trcmApiKey);
        $trcmSecretKey = trim($_POST['trcm_key_secret']);
        update_option('trcm_key_secret', $trcmSecretKey);

        $trcmTidWelcome = trim($_POST['trcm_tid_welcome']);
        update_option('trcm_tid_welcome', $trcmTidWelcome);
        $trcmTidPayment = trim($_POST['trcm_tid_payment']);
        update_option('trcm_tid__payment', $trcmTidPayment);
        $trcmTidPledge = trim($_POST['trcm_tid_pledge']);
        update_option('trcm_tid_pledge', $trcmTidPledge);

        $trcmTextWelcome = $_POST['trcm_text_welcome'];
        update_option('trcm_text_welcome', $trcmTextWelcome);
        $trcmTextPayment = $_POST['trcm_text_payment'];
        update_option('trcm_text_payment', $trcmTextPayment);
        $trcmTextPledge = $_POST['trcm_text_pledge'];
        update_option('trcm_text_pledge', $trcmTextPledge);
        ?>
        <div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div>
        <?php
    } 
    else 
    {
        $trcmFromName = get_option('trcm_from_name');
        $trcmFromEmail = get_option('trcm_from_email');

        $trcmApiKey = get_option('trcm_key_api');
        $trcmSecretKey = get_option('trcm_key_secret');

        $trcmTidWelcome = get_option('trcm_tid_welcome');
        $trcmTidPayment = get_option('trcm_tid_payment');
        $trcmTidPledge = get_option('trcm_tid_pledge');

        $trcmTextWelcome = get_option('trcm_text_welcome');
        $trcmTextPayment = get_option('trcm_text_payment');
        $trcmTextPledge = get_option('trcm_text_pledge');
    }

    // Include settings page
    include('trcm_settings.php');
}


// Declare email method
// Using: https://github.com/mailjet/mailjet-apiv3-php

/**
 * Send email with MailJet Send API
 * 
 * @param string    $templateKey
 * @param string    $mailTo
 * @param array     $mergeFields
 * @return bool
 */
function trcm_mail($templateKey, $mailTo, $mergeFields = array()) {
    // Return value
    $emailSent = false;


    // Gather data for error logging
    $errorData = func_get_args();


    // Tidy up data
    $mailTo = trim($mailTo);
    $name = trim($mergeFields['name']);
    if (!$name) {
        $name = "customer";
        trcm_log_error("Name not set", $errorData);
    }
    $firstName = trim($mergeFields['firstname']);
    if (!$firstName) {
        $firstName = $name;
        trcm_log_error("First name not set", $errorData);
    }
    

    // Get client
    require __DIR__.'/vendor/autoload.php';
    $client = new Mailjet\Client(get_option('trcm_key_api'), get_option('trcm_key_secret'));
    if (!$client) {
        trcm_log_error("Could not create client", $errorData);
        return $emailSent;
    }


    // Get template
    $templateId = get_option('trcm_tid_' . $templateKey);
    if (!$templateId) {
        trcm_log_error("Invalid template key", $errorData);
        return $emailSent;
    }
    $template = null;
    $templateResp = $client->get(Mailjet\Resources::$TemplateDetailcontent, ['id' => $templateId]);
    if ($templateResp->success()) $template = $templateResp->getData()[0];
    if (!$template) {
        trcm_log_error("Template not found", $errorData);
        return $emailSent;
    }


    // Send email
    $email = [
        'FromName' => get_option('trcm_from_name'),
        'FromEmail' => get_option('trcm_from_email'),
        'Subject' => $template['Headers']['Subject'],
        'MJ-TemplateLanguage' => true,
        'Html-part' => $template['Html-part'],
        'Recipients' => [['Email' => $mailTo]],
        'Vars' => $mergeFields
    ];
    if (get_option('trcm_text_' . $templateKey)) $email['Text-part'] = $template['Text-part'];
    
    $emailResp = $client->post(Mailjet\Resources::$Email, ['body' => $email]);
    $emailSent = $emailResp->success();
    if (!$emailSent) {
        trcm_log_error("Email not sent", $errorData);
        return $emailSent;
    }


    // Update contact
    try {
        // Get contact
        $contactGetResp = $client->get(Mailjet\Resources::$Contact, ['id' => $mailTo]);

        if (!$contactGetResp->success()) {
            trcm_log_error("Contact not found", $errorData);
        } else {
            $contact = $contactGetResp->getData()[0];
            $contactId = $contact['ID'];

            if ($contact['Name'] != $name) {
                // Update contact
                $contactUpd = [
                    'name' => $name,
                ];
                $contactUpdResp = $client->put(Mailjet\Resources::$Contact, ['id' => $contactId, 'body' => $contactUpd]);
                if (!$contactUpdResp->success() && $contactUpdResp->status != 304) 
                    trcm_log_error("Contact udpate failed", $errorData);
            }


            // Update contact data
            $contactDataUpd = [
                'Data' => [
                    [
                        'Name' => "name",
                        'value' => $name
                    ],
                    [
                        'Name' => "firstname",
                        'value' => $firstName
                    ]
                ]
            ];
            $contactDataUpdResp = $client->put(Mailjet\Resources::$Contactdata, ['id' => $contactId, 'body' => $contactDataUpd]);
            if (!$contactDataUpdResp->success()) 
                trcm_log_error("Contact data udpate failed", $errorData);
        }

    } catch (Exception $ex) {
        trcm_log_error("Contact udpate exception: " . $ex->getMessage(), $errorData);
    }

    // Email send complete
    return $emailSent;
}



// Log errors
function trcm_log_error($message, $data = array()) {
    $message = "TRC Mailjet - " . $message;
    if ($data && count($data) > 0)
        $message .= " [" . print_r($data, true) . "]";

    error_log($message);
    if (defined('WP_DEBUG') && WP_DEBUG) 
		echo '<div class="updated"><p><strong>' . $message . '</strong></p></div>';
}

