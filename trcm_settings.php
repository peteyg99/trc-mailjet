<div class="wrap">
    <h2>TRC Mailjet Settings</h2>
     
    <form name="trcm_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

        <input type="hidden" name="trcm_hidden" value="Y">
        <?php wp_nonce_field('trcm-options'); ?>
        
        <div style="color: #000; background: #fff; padding: 10px; margin: 10px 0;">
            <h3>API Settings</h3>

            <p>
                To find these values visit: <a href="https://app.mailjet.com/account/api_keys" target="_blank">API Key Management</a>.
            </p>

            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row"><label for="trcm_key_api">API key</label></th>
                        <td>
                            <input type="text" name="trcm_key_api" id="trcm_key_api" value="<?php echo $trcmApiKey; ?>" class="regular-text code" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="trcm_key_secret">Secret key</label></th>
                        <td>
                            <input type="text" name="trcm_key_secret" id="trcm_key_secret" value="<?php echo $trcmSecretKey; ?>" class="regular-text code" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>        

        <div style="color: #000; background: #fff; padding: 10px; margin: 10px 0;">
            <h3>Email Settings</h3>

            <p>
                These settings will override the settings in your templates.
            </p>

            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row"><label for="trcm_from_name">From name</label></th>
                        <td>
                            <input type="text" name="trcm_from_name" id="trcm_from_name" value="<?php echo $trcmFromName; ?>" class="regular-text code" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="trcm_from_email">From email</label></th>
                        <td>
                            <input type="text" name="trcm_from_email" id="trcm_from_email" value="<?php echo $trcmFromEmail; ?>" class="regular-text code" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="color: #000; background: #fff; padding: 10px; margin: 10px 0;">
            <h3>Template IDs</h3>

            <p>
                The Template ID can be found by visiting the <a href="https://app.mailjet.com/templates/marketing" target="_blank">templates area</a> of your Mailjet account.
            </p>

            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row"><label for="trcm_tid_welcome">Welcome</label></th>
                        <td>
                            <p>
                                <input type="text" name="trcm_tid_welcome" id="trcm_tid_welcome" value="<?php echo $trcmTidWelcome; ?>" class="regular-text code" />
                            </p>
                            <p>
                                <label for="trcm_text_welcome">
                                    <input name="trcm_text_welcome" type="checkbox" id="trcm_text_welcome" value="1"<?php echo $trcmTextWelcome ? ' checked' : ''; ?> />
                                    Send text version
                                </label>
                            </p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="trcm_tid_payment">Payment</label></th>
                        <td>
                            <p>
                                <input type="text" name="trcm_tid_payment" id="trcm_tid_payment" value="<?php echo $trcmTidPayment; ?>" class="regular-text code" />
                            </p>
                            <p>
                                <label for="trcm_text_payment">
                                    <input name="trcm_text_payment" type="checkbox" id="trcm_text_payment" value="1"<?php echo $trcmTextPayment ? ' checked' : ''; ?> />
                                    Send text version
                                </label>
                            </p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><label for="trcm_tid_pledge">Pledge</label></th>
                        <td>
                            <p>
                                <input type="text" name="trcm_tid_pledge" id="trcm_tid_pledge" value="<?php echo $trcmTidPledge; ?>" class="regular-text code" />
                            </p>
                            <p>
                                <label for="trcm_text_pledge">
                                    <input name="trcm_text_pledge" type="checkbox" id="trcm_text_pledge" value="1"<?php echo $trcmTextPledge ? ' checked' : ''; ?> />
                                    Send text version
                                </label>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <p class="submit">
            <input type="submit" name="Submit" value="Update Options" class="button button-primary" />
        </p>
    </form> 
</div>