# Mailjet plugin for The Right Crowd



## Merge fields

* name
* firstname (defaults to name)
* reference
* opportunity
* amount


## Methods

### trcm_mail( $templateKey, $mailTo, $mergeFields = array() )

Returns: bool -> email sent

